//
//  ViewController.m
//  BascisOfObjective-C
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "XYZPerson.h"

@interface XYZPerson ()

@property (readwrite) float height;
@property (readwrite) float weight;

@end

@implementation XYZPerson

- (void)sayHello
{
    self.firstName = @"Mehul";
    self.lastName = @"Makwana";
    self.height = 5.11;
    self.weight = 68.0;
    NSLog(@"My name is %@ %@", self.firstName,self.lastName);
}
- (void)saySomething:(NSString *)greeting
{
    NSLog(@"%@",greeting);
}
+ (id)person{
    return [[self alloc]init];
}
- (id)initWithFirstName:(NSString *)aFirstName lastName:(NSString *)aLastName dateOfBirth:(NSDate*)aDate
{
    self = [super init];
    
    if (self) {
        _firstName = aFirstName;
        _lastName = aLastName;
        _dateOfBirth = aDate;
    }
    
    return self;
}
-(void)measueHeightAndWeight
{
    NSLog(@"Hello %@ %@ your height is %f feet and weight is %f kg",self.firstName,self.lastName, self.height, self.weight);
}
@end
