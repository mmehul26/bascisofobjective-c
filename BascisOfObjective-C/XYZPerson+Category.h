//
//  XYZPerson+Category.h
//  BascisOfObjective-C
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "XYZPerson.h"

@interface XYZPerson (Category)

-(void)firstLastNameWith:(NSString *)firstName lastName :(NSString *)lastName;

-(void)lastFirstNameWith:(NSString *)lastName firstName :(NSString *)firstName;


@end
