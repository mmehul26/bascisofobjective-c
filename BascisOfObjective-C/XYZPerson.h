//
//  ViewController.h
//  BascisOfObjective-C
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYZPerson : NSObject

@property NSString *firstName;
@property NSString *lastName;
@property NSDate *dateOfBirth;

@property (readonly) float height;
@property (readonly) float weight;

-(void)sayHello;

-(void)saySomething:(NSString *)greeting;

+(id)person;

- (id)initWithFirstName:(NSString *)aFirstName lastName:(NSString *)aLastName dateOfBirth:(NSDate*)aDate;


-(void)measueHeightAndWeight;

@end

