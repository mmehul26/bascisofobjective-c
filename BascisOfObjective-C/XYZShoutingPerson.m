//
//  XYZShoutingPerson.m
//  BascisOfObjective-C
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "XYZShoutingPerson.h"

@implementation XYZShoutingPerson

- (void)saySomething:(NSString *)greeting{
    NSString *newString = [greeting uppercaseString];
    NSLog(@"%@",newString);
}

@end
