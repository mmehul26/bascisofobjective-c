//
//  XYZPerson+Category.m
//  BascisOfObjective-C
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "XYZPerson+Category.h"

@implementation XYZPerson (Category)

-(void)firstLastNameWith:(NSString *)firstName lastName :(NSString *)lastName
{
    NSLog(@"%@ %@",firstName,lastName);
}

-(void)lastFirstNameWith:(NSString *)lastName firstName :(NSString *)firstName
{
    NSLog(@"%@ %@",lastName,firstName);
}

@end
