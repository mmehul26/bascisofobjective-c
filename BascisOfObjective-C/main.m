//
//  main.m
//  BascisOfObjective-C
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "XYZPerson.h"
#import "XYZShoutingPerson.h"
#import "XYZPerson+Category.h"
int main(int argc, char * argv[]) {
    
    //Object of Class XYZPerson
    XYZPerson *somePerson = [[XYZPerson alloc]init];
    
    //Called sayHello Method
    [somePerson sayHello];
    
    //Called saySomething Method
    [somePerson saySomething:@"Mehul"];
    
    //Called Person Class Factory Method
    NSLog(@"%@", [XYZPerson person]);
    
    //New XYZPerson object
    XYZPerson *NewSomePerson;
    if (!NewSomePerson) {
        NSLog(@"Class object is automatically set to nil");
    }
    
    XYZPerson *initObj = [[XYZPerson alloc]initWithFirstName:@"Ma" lastName:@"kwana" dateOfBirth:[NSDate date]];
    
    NSLog(@"Hello %@%@",initObj.firstName,initObj.lastName);
    
    [somePerson firstLastNameWith:@"Mehul" lastName:@"Makwana"];
    
    [somePerson lastFirstNameWith:@"Makwana" firstName:@"Mehul"];
    
    [somePerson measueHeightAndWeight];
    
    //Object of XYsaZShoutingPerson
    XYZShoutingPerson *anotherPerson = [[XYZShoutingPerson alloc]init];
    
    //Called saySomething Method of Parent Class
    [anotherPerson saySomething:@"makwana"];
    
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
